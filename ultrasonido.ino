#include <NewPing.h>
#include <Servo.h>  

#undef REFRESH_INTERVAL

#define REFRESH_INTERVAL 16630 //Redefinicion para que la frequencia sea 60Hzs

#define ULTRASONICPIN 6
#define ECHOPIN 5
#define MAXDISTANCE 500
#define SIGNALPIN 10
#define ESCPIN 11
#define BACKWARDS 1320
#define STOP_MESSAGE_COUNT 20
#define ITER_SEQ_REVERSE 25000

int sig;
int distance;
int sdf;
int sign = 1500;
Servo motor;
boolean first = true;
 
NewPing sonar(ULTRASONICPIN, ECHOPIN, MAXDISTANCE);
 
void setup() {
  Serial.begin(9600);
  motor.attach(ESCPIN);
  pinMode(SIGNALPIN, INPUT);
}
 
void loop() {
  delay(30);  // esperar 50ms entre pings (29ms como minimo)
  distance = sonar.ping_cm();
  if(distance < 50 && distance > 5){
    if(sig > 1500){
      sig = 1500;  
    }
    else{
      sdf = pulseIn(SIGNALPIN, HIGH);
      sig = getTransformSignal(sdf);
      if(sig > 1500){
        sig = 1500;
      }
    }
  }
  else{
    sdf = pulseIn(SIGNALPIN, HIGH);
    sig = getTransformSignal(sdf);
  }
  motor.writeMicroseconds(sig);
  //Serial.println(sonar.ping_cm());
  //Serial.println(sdf, DEC);
  
  
}

int getTransformSignal(int value){
    //Serial.print("Value: ");
    //Serial.println(value);
    if (value >= 1500) {
      sign = map(value, 1500, 2000, 1510, 1560);
      first = true;
    }
    else if (value < 1400) {
      if(first) {
        seqAtras();
        first = false;
        //Serial.println("------------------------------");
      }
      sign = map(value, 1000, 1400, 1380, 1400);
    }
    else {
      sign = 1500;
    }
   //Serial.print("Maped: ");
   //Serial.println(sign);
   //Serial.println();
    return sign;
}

void seqAtras(){
  for(int i = 0; i < ITER_SEQ_REVERSE; i++) motor.writeMicroseconds(1560);
  delay(1);
  //Serial.println("A");
  for(int i = 0; i < ITER_SEQ_REVERSE; i++) motor.writeMicroseconds(1500); 
  delay(1);
  //Serial.println("S");
  for(int i = 0; i < ITER_SEQ_REVERSE; i++) motor.writeMicroseconds(1000); 
  delay(1);
  //Serial.println("AT");
  for(int i = 0; i < ITER_SEQ_REVERSE; i++) motor.writeMicroseconds(1500);
  delay(1);
  //Serial.println("S ");
}
